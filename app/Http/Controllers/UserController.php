<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /*
     * 個人介紹頁面
     */
    public function show(User $user)
    {

        // 個人文章
        $posts = $user->posts()->orderBy('created_at', 'desc')->take(10)->get();
        // 個人關注／粉絲／文章
        $user = \App\User::withCount(['stars', 'fans', 'posts'])->find($user->id);
        $fans = $user->fans()->get();
        $stars = $user->stars()->get();

        return view("user/show", compact('user', 'posts', 'fans', 'stars'));
    }

    public function fan(User $user)
    {
        $me = \Auth::user();
        \App\Fan::firstOrCreate(['fan_id' => $me->id, 'star_id' => $user->id]);
        return [
            'error' => 0,
            'msg' => ''
        ];
    }

    public function unfan(User $user)
    {
        $me = \Auth::user();
        \App\Fan::where('fan_id', $me->id)->where('star_id', $user->id)->delete();
        return [
            'error' => 0,
            'msg' => ''
        ];
    }

    public function setting()
    {
        $me = \Auth::user();
        return view('user/setting', compact('me'));
    }

    public function settingStore(Request $request, User $user)
    {
        $this->validate(request(),[
            'name' => 'min:3',
        ]);

        $name = request('name');
        if ($name != $user->name) {
            if(\App\User::where('name', $name)->count() > 0) {
                return back()->withErrors(array('message' => '用戶名稱已經被註冊'));
            }
            $user->name = request('name');
        }
        if ($request->file('avatar')) {
           

            $file = $request->file('avatar');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/', $filename);
            $user->avatar = $filename;
            //$path = $request->file('avatar')->storePublicly(md5(\Auth::id() . time()));
            //$user->avatar = "/storage/". $path;
        }

        $user->save();
        return back();
    }
}
