<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /*
     * 消息頁面
     */
    public function index()
    {
        // 獲取我收到的消息
        $user = \Auth::user();
        $notices = $user->notices;
        return view("notice/index", compact('notices'));
    }
}
