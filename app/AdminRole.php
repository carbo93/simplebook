<?php

namespace App;


class AdminRole extends Model
{
    protected $table = "admin_roles";

    /*
     * 當前角色的所有權限
     */
    public function permissions()
    {
        return $this->belongsToMany(\App\AdminPermission::class, 'admin_permission_role', 'permission_id', 'role_id')->withPivot(['permission_id', 'role_id']);
    }

    /*
     * 給角色授權
     */
    public function grantPermission($permission)
    {
        return $this->permissions()->save($permission);
    }

    /*
     * 刪除role和permission的關聯
     */
    public function deletePermission($permission)
    {
        return $this->permissions()->detach($permission);
    }

    /*
     * 角色是否有權限
     */
    public function hasPermission($permission)
    {
        return $this->permissions->contains($permission);
    }
}
