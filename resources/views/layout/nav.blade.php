<div class="blog-masthead">
    <div class="container">
        <form action="/posts/search" method="GET">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <a class="blog-nav-item " href="/posts">首頁</a>
            </li>
            <li>
                <a class="blog-nav-item" href="/posts/create">寫文章</a>
            </li>
            <li>
                <a class="blog-nav-item" href="/notices">通知</a>
            </li>
            <!--
            <li>
                <input name="query" type="text" value="@if(!empty($query)){{$query}}@endif" class="form-control" style="margin-top:10px" placeholder="請輸入要搜尋的文字">
            </li>
            <li>
                <button class="btn btn-default" style="margin-top:10px" type="submit">查詢</button>
            </li>
            -->
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <div>
                    <i class="header-icon icon-account"></i>
                    <img src="../uploads/{{$user->avatar}}" alt="" class="img-rounded" style="border-radius:500px; height: 30px">
                    <a href="#" class="blog-nav-item dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$user->name}}  <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/user/{{$user->id}}">我的主頁</a></li>
                        <li><a href="/user/{{$user->id}}/setting">個人設置</a></li>
                        <li><a href="/logout">登出</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        </form>
    </div>
</div>