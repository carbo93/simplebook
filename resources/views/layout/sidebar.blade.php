<div id="sidebar" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">


    <aside id="widget-welcome" class="widget panel panel-default">
        <div class="panel-heading">
            文章搜尋
        </div>
        <div class="panel-body">
            <p>        
                <form action="/posts/search" method="GET">
                    <input name="query" type="text" value="@if(!empty($query)){{$query}}@endif" class="form-control" style="margin-top:10px" placeholder="請輸入要搜尋的文字">
                    <button class="btn btn-default" style="margin-top:10px" type="submit">查詢</button>
                </form>
            </p>
        </div>
    </aside>
    <aside id="widget-welcome" class="widget panel panel-default">
        <div class="panel-heading">
            歡迎！
        </div>
        <div class="panel-body">
            <p>
                歡迎光臨
            </p>
            <p>
                <strong><a href="/">易書網</a></strong>
            </p>
            <div class="bdsharebuttonbox bdshare-button-style0-24" data-bd-bind="1494580268777"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_douban" data-cmd="douban" title="分享到豆瓣网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_bdhome" data-cmd="bdhome" title="分享到百度新首页"></a></div>

        </div>
    </aside>
    <aside id="widget-categories" class="widget panel panel-default">
        <div class="panel-heading">
            專題
        </div>

        <ul class="category-root list-group">
            @foreach($topics as $topic)
                <li class="list-group-item">
                    <a href="/topic/{{$topic->id}}">{{$topic->name}}
                    </a>
                </li>
            @endforeach
        </ul>

    </aside>
</div>
</div>
