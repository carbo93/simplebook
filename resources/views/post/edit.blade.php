@extends("layout.main")

@section("content")

    <div class="col-sm-8 blog-main">
        <form action="/posts/{{$post->id}}" method="POST">
            {{method_field("PUT")}}
            {{csrf_field()}}
            <div class="form-group">
                <label>標題</label>
                <input name="title" type="text" class="form-control" placeholder="請填入標題文字" value="{{$post->title}}">
            </div>
            <div class="form-group">
                <label>内容</label>
                <textarea id="content" name="content" class="form-control" style="height:400px;max-height:500px;"  placeholder="請填入內容文字">{{$post->content}}</textarea>
            </div>
            <button type="submit" class="btn btn-default">送出</button>
        </form>
        <br>
        @include('layout.error')
    </div><!-- /.blog-main -->


@endsection