## Laravel
執行安裝套件

```bash
sudo composer update
```

執行數據轉移

```bash
sudo php artisan migrate
```

等安裝好Elasticsearch執行以下程式碼可以同步資料庫的資料到Elasticsearch 

```bash
php artisan scout:import "App\Post"
```

## 安装 JAVA 8

添加Oracle JavaPPA 到 apt

```bash
sudo add-apt-repository -y ppa:webupd8team/java
```

更新apt包數據庫

```bash
sudo apt-get update
```

安裝穩定版 Oracle Java8（接收彈出的協議內容）：

```bash
sudo apt-get -y install oracle-java8-installer
```

安装完成


## 手動安装 Elasticsearch

產生一個資料夾
```bash
sudo mkdir /etc/elasticsearch2/
```

下載Elasticsearch 2.4.6
```bash
curl -L -O https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.6/elasticsearch-2.4.6.tar.gz
```

解壓縮
```bash
tar -xvf elasticsearch-2.4.6.tar.gz
```

切換目錄
```bash
cd elasticsearch-2.4.6/bin
```

啟動
```bash
./elasticsearch
```


elasticsearch 不可以被root 帳號執行所以要更改ＯＷＮＥＲ
```bash
sudo chown -R myuser:mygroup /etc/elasticsearch2/elasticsearch-2.4.6
```

如無法啟動下以下指令
```bash
netstat -nltp
```
找到9200 9300被佔用port的pid然後刪除再重新啟動一次elasticsearch


其他安裝錯誤參考這裡
https://www.jianshu.com/p/365db8b181cc



## 自動安装 Elasticsearch(不建議)

導入 Elasticsearch 公共 GPG 密鑰到 apt


```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```

建立 Elasticsearch 資源列表

```bash
sudo apt-get install apt-transport-https
```
```bash
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
```

更新 apt 包數據庫

```bash
sudo apt-get update && sudo apt-get install elasticsearch
```

安装完成，接下来編輯配置文件

```bash
sudo vi /etc/elasticsearch/elasticsearch.yml
```

```bash
network.host: localhost
```

保存並退出 elasticsearch.yml

開啟 Elasticsearch

```bash
sudo service elasticsearch restart
```

運行以下下命令將在系统啟動時啟動 Elasticsearch

```bash
sudo update-rc.d elasticsearch defaults 95 10
```